# INTRODUCTION À LIVEPOSE
Emmanuel Durand, Bruno Colpron & Victor Rios
*25 mai 2023*

## DOCUMENTATION

* https://sat-mtl.gitlab.io/documentation/livepose/fr/index.html
* https://gitlab.com/sat-mtl/tools/livepose
* https://sat.qc.ca/fr/metalab
* https://sat-mtl.gitlab.io/fr/

## Présentations

### Emmanuel Durand

<img src="public/images/manu.png" width="15%">

Manu est l'un des co-directeurs du Metalab, le laboratoire de recherche de la [SAT]. Chercheur et codeur de formation, il s'intéresse principalement à tout ce qui touche à la 3D, à l'apprentissage automatique et à la photographie computationnelle. Emmanuel est le créateur et l'auteur principal de Splash.

### Bruno Colpron

<img src="public/images/bruno.jpg" width="15%">

Bruno a collaboré à plusieurs productions pour dômes et VR qui ont été présentées à la [SAT], au Planétarium Rio Tinto Alcan ainsi qu’à l’international. Ses plus récents travaux avec l'ONF font appel à des stratégies immersives dans des contextes atypiques. Depuis l’été 2022, Bruno collabore au Metalab à titre d’intégrateur d’installations immersives.

### Victor Rios

<img src="public/images/profil.jpeg" width="15%">

Victor travaille au Metalab en tant que stagiaire depuis Décembre 2022. Il s'intéresse principalement aux réseaux de neuronnes artificiels en facilitant leur intégration aux outils de la [SAT]. Son travail s'articule en grande partie autour du logiciel LivePose, développé au Metalab.

### La Société des arts technologiques [SAT]

<img src="public/images/sat.jpg" width="40%">

Depuis sa création en 1996, la SAT est un centre transdisciplinaire de recherche et création, de production, de formation et de diffusion voué au développement et à la conservation de la culture numérique. 

### Le Metalab

<img src="public/images/Metalab_Logo_Blanc_dégradé_.png" width="10%">

Le Metalab est le laboratoire de recherche et développement de la Société des arts technologiques. Sa mission est double : stimuler l’émergence d’expériences immersives innovantes et rendre leur conception accessible aux artistes de l’immersion à travers un écosystème de logiciels libres.

### LivePose

<img src="public/images/livepose_logo.png" width="20%">

LivePose est un outil à interface en ligne de commande pour reconnaître des silhouettes et des actions à partir de flux vidéo, filtrer les informations extraites et envoyer les résultats à travers des réseaux. LivePose traite les flux vidéo en direct et transmet les résultats pour chaque trame.

LivePose vise à rendre accessibles les travaux de recherche récents autour de la détection de silhouette à l’aide de réseaux de neurones. Ces nouvelles méthodes sont souvent fastidieuses à utiliser et déployer, d'autant plus que plusieurs projets logiciels issus de projets de recherche ne sont pas maintenus dans le temps. Dans certains cas, les licences empêchent l’utilisation dans des projets commerciaux, dont certaines performances artistiques. En outre, les cadriciels de réseaux de neurones profonds ont leurs propres enjeux pour leur déploiement et maintenance. LivePose se centre sur l’implémentation d’algorithmes autour de bibliothèques solides.

## INTRODUCTION

LivePose vise à rendre accessibles les travaux de recherche récents autour de la détection de silhouette à l’aide de réseaux de neurones. Ces nouvelles méthodes sont souvent fastidieuses à utiliser et déployer, d'autant plus que plusieurs projets logiciels issus de projets de recherche ne sont pas maintenus dans le temps. Dans certains cas, les licences empêchent l’utilisation dans des projets commerciaux, dont certaines performances artistiques. En outre, les cadriciels de réseaux de neurones profonds ont leurs propres enjeux pour leur déploiement et maintenance. LivePose se centre sur l’implémentation d’algorithmes autour de bibliothèques solides.

## INSTALLATION

Installation à partir des sources:

**Cloner & installer LivePose**
* Suivre instructions: https://gitlab.com/sat-mtl/tools/livepose/-/blob/main/doc/Installation.md
* Cloner le repo LivePose
* Installer les dépendances
* Installer le support de caméra Intel RealSense
* Assurez-vous d’avoir les derniers drivers Nvidia
* Créer un environnement virtuel

## INSTALLATION (suite)

**Cloner le repo mmaction2-creo**

Repo privé - nécessite un compte GitLab (que nous avons approuvé)
* À partir du dossier de LivePose, rouler: 
`git clone https://gitlab.com/sat-mtl/collaborations/mmaction2-creo.git`
* Tester LivePose
À partir du dossier de LivePose, rouler:
`./livepose.sh -c ./mmaction2-creo/configs/livepose/frame_liveaction_formation.json`

Il est possible que des dépendances soient manquantes, si c’est le cas, les installer

Exemples:
pip3 install mmaction2
pip3 install mmengine
(...)

## IDENTIFICATION DE LA CAMÉRA INTEL REALSENSE

Plusieurs méthodes permettent de valider la connexion d’une ou plusieurs caméras Intel RealSense ainsi que les flux y étant associés.
* En utilisant la commande v4l2-ctl --list-devices dans un terminal.
* En ouvrant un périphérique de capture dans VLC.
* Avec l’outil realsense-viewer suite à l’installation du SDK fourni par Intel.

## L’OUTIL REALSENSE VIEWER

Cet outil permet en autre de :
* Visualiser les flux d’une ou plusieurs caméras Intel RealSense
* Valider le débit de connexion
* Gérer les paramètres des caméras
* Jouer une séquence enregistrée en format rosbag

## ENREGISTREMENT D’UN JEU DE DONNÉES (dataset)

### RS-MULTICAM-RECORD

Outil en ligne de commande permettant l’enregistrement simultané des flux provenant de plusieurs caméras Intel RealSense

Installation à partir du code source disponible sur la page GitLab du Metalab

Valider d'abord que les caméras Intel RealSense sont bien reconnues par votre système et que la connexion a été établie en USB 3. (Attention aux rallonges USB !) 

Il est idéal de créer une arborescence de fichier avant de débuter l’enregistrement de votre jeu de données (en anglais, dataset).

Exemple: dataset/séquence_#/action_#/

Les fichiers Rosbag créés par rs-multicam-record porteront des noms uniques, car ils incluent la date, l’heure ainsi que le numéro de série de la caméra.

## ENREGISTREMENT D’UN JEU DE DONNÉES (dataset)

### RS-MULTICAM-RECORD

Flags:

`-c` : Enregistrement du flux RGB
`-d` : Enregistrement du flux de profondeur
`-i` : Enregistrement du flux infrarouge
`--fps` : Spécifier la cadence d’image
`--rgb-size` : Spécifier la résolution du flux RGB
`--ir-size` : Spécifier la résolution du flux infrarouge
`-f` : Spécifier le chemin d’enregistrement (par défaut : ./)
`-t` : Spécifier la durée de l’enregistrement (par défaut: -1)

Exemple de ligne de commande:

`./rs-multicam-record -c -i -d --fps 30 --rgb-size 640x480 --ir-size 640x480 -f /dataset/sequence/action -t 30`

Notes:
Ici, rs-multicam-record est lancé à partir du dossier build créé à partir des sources.
LivePose préférant le ratio 4:3, la résolution des flux est spécifié à 640x480.
Il est judicieux de créer plusieurs enregistrements de courtes durées (ici 30 secondes) pour faciliter le traitement et éviter de faire le même geste à répétition sans pause.

## UTILISATION

Une fois LivePose installé, voici comment récupérer la liste de toutes ses options:

`livepose --help`

Un fichier de configuration par défaut se trouve dans le dossier configs localisé à la racine du dépôt.

## CONFIGURATION

Voici un exemple de fichier de configuration pour LivePose pour transmettre toutes les données de silhouettes détectées avec la première caméra Video4Linux vers un hôte distant d’adresse IP 192.168.2.144, via OSC:

```
{
    "backend": {
        "name": "posenet",
        "params": {
            "device_id": 0
        }
    },
    "cameras": {
        "input_paths":["/dev/video0"],
        "camera_matrices": {}
    },
    "filters": {
        "skeletons": {}
    },
    "outputs": {
        "osc": {
            "destinations": {
                "192.168.2.144": 9000
            }
        }
    }
}

```

Une fois la configuration enregistrée dans un fichier `/path/to/the/config/file.json` , voici la commande à exécuter dans un terminal:

`livepose --config /path/to/the/config/file.json`

Après une initialisation de courte durée, une fenêtre affichera le flux de la caméra avec les silhouettes détectées surimposées, et les messages OSC pourront être reçus sur le port 9000 de l’hôte distant.

## MODÈLE DE RECONNAISSANCE D’ACTION (HAR)

Dans le but de construire notre configuration LivePose, nous nous intéressons ici à l’entraînement et au déploiement d’un modèle de reconnaissance d’action (HAR).

Le modèle utilisé est un réseau de neurones qui détecte les features spatiales (courbes, formes, couleurs) et temporelles (en fonction des frames précédentes) sur une séquence d’images et renvoie la classe d’action qu’il reconnaît. Ce réseau est appelé I3D et on l’entraîne à travers la toolbox MMaction2 qui nous permet d’accéder facilement au modèle et à son entraînement.

## MODÈLE DE RECONNAISSANCE D’ACTION (HAR)

Le modèle I3D prend en entrée une séquence d’image et renvoie une étiquette de classe:

End-to-end deep learning for recognition of ploidy status using time-lapse videos - Scientific Figure on ResearchGate. 

Available from: https://www.researchgate.net/figure/The-I3D-model-process-map-Our-input-models-are-RGB-I3D-and-Optical-flow-I3D-For-the-RGB_fig3_351781698 [accessed 8 May, 2023]

## CONCEPTION ET SÉLECTION DES ACTIONS

* Dans un premier temps, il faut définir le problème à résoudre et les actions à effectuer. 

Il faut choisir des actions faciles à reproduire et assez différentes entre elles pour que le modèle les différencie. (Inter-class error)

* Définir un environnement spécifique (luminosité, exposition) pour l'entraînement. 

Si vous voulez que le modèle généralise bien à différents milieux il faut l'entraîner dans des environnements changeants.

* Donner une grande diversité à son jeu de données est primordial afin d’aider le modèle à généraliser sa reconnaissance sur des personnes inconnues. 

Il est donc préférable de créer un jeu de données avec des personnes différentes (genre, ethnie, taille, vêtements) et dans leur singularité profiter des légères variations entre les mouvements de chacun pour rendre le modèle plus robuste.

## AUGMENTATION DES DONNÉES

Les modèles d'apprentissage profond (deep learning) ont besoin de grandes quantités de données pour être efficaces. Lorsqu’on crée un jeu de données à la main, il se peut que la quantité de données ne soit pas suffisante et que l'on doive augmenter la taille du jeu de données.

Ce processus permet d’augmenter la taille du jeu de données en créant artificiellement des données à partir des vidéos déjà capturées. L’augmentation repose sur l’application de transformations comme des rotations, des agrandissements, des zooms, des rognages, l’ajout de bruit, ajoutant de la variation dans les vidéos.

## AUGMENTATION DES DONNÉES

Voici quelques propositions pour des couches d’augmentation : 

* Salt and pepper : des pixels noirs et blancs ajoutent du bruit sur l'image;
* Sharpen : accentue la délimitation des formes;
* Emboss : accentue le contraste des formes;
* Flip : inverse l'image horizontalement;
* Affine : applique des transformations affines aux images, comme un changement d'échelle, des translations ou des rotations.

Cette augmentation permet une meilleure généralisation du modèle sur des exemples nouveaux pour celui-ci.


## TUTORIEL MMACTION2

Entraînement avec mmaction2 à partir du jeu de données :

`2022-2023-augmented-CREO-Megaceta`

Ce tutoriel provient de la documentation de mmaction2 et modifié par Christian Frisson et Victor Rios afin d’être adapté à LivePose.

## TUTORIEL MMACTION2: System

### nvidia apt repo

```
OS="ubuntu2204" && \
wget https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/cuda-${OS}.pin && \
sudo mv cuda-${OS}.pin /etc/apt/preferences.d/cuda-repository-pin-600 && \
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/3bf863cc.pub && \
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/ /" && \
sudo apt-get update
```

## TUTORIEL MMACTION2: System

### mpa-jammy-amd64-nvidia

```
sudo apt install -y coreutils wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-jammy-amd64-nvidia/sat-metalab-mpa-keyring.gpg \
	| gpg --dearmor \
	| sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-jammy-amd64-nvidia/debs/ sat-metalab main main/debug' \
	| sudo tee /etc/apt/sources.list.d/sat-metalab-mpa.list && \
echo 'deb [ arch=amd64, signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
	| sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list && \
sudo apt update
```

## TUTORIEL MMACTION2: System

### mmaction2

```
sudo apt install cuda-nvcc-11-7 cuda-nvrtc-dev-11-7 ffmpeg libcufile-11-7 python3.10-venv python3-mmcv python3-opencv python3-seaborn python3-torchvision python3-tqdm
sudo ldconfig
```

## TUTORIEL MMACTION2: System

### Clones of git repositories

In ~/livepose/:
```
git clone https://gitlab.com/sat-mtl/collaborations/mmaction2-creo -b data/2022-CREO-Megaceta/*
git clone https://gitlab.com/sat-mtl/tools/forks/mmpose.git
git clone https://gitlab.com/sat-mtl/tools/forks/mmdetection.git
```

## TUTORIEL MMACTION2: System

### pip dependencies

In ~/livepose/mmaction2-creo:
```
python3 -m venv --system-site-packages --symlinks mmaction2_venv
source ~/livepose/mmaction2_venv/bin/activate
pip install -e .
```

## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 1 : Structurer le dossier de données

Dans livepose/mmaction2-creo/, l’arborescence du dossier contenant les données peut-être représentée comme ceci :

```
data
└── dataset_name/
    ├── videos/
    │   ├── class_00/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   ├── class_01/
    │   │   ├── video_name1.mp4
    │   │   ├── video_name2.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   └── class_02/
    │       └── ...
    └──
```

Ainsi, on sépare chaque vidéo à partir de la classe à laquelle elle appartient.

## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 2 : Convertir les vidéos en images

Le modèle que l’on utilise s’entraîne à partir d’une séquence d’images représentant une vidéo. 

Ainsi, la commande python video_to_rawframes.py dataset_name permet cette conversion dans le dossier rawframes.

```
data
└── dataset_name/
    ├── videos/
    │   ├── class_00/
    │   │   ├── video_name1.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   ├── class_01/
    │   │   ├── video_name1.mp4
    │   │   ├── ....
    │   │   └── video_nameN.mp4
    │   └── class_02/
    │       └── ...
    ├── rawframes/
    │   ├── class_00/
    │   │   └── video_name1/
    │   │       ├── img_00001.jpg
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg
    │   │   └── ...
    │   │   └── video_nameN/
    │   │       ├── img_00001.jpg
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg
    │   ├── class_01/
    │   │   └── video_name1/
    │   │       ├── img_00001.jpg
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg
    │   │   └── ...
    │   │   └── video_nameN/
    │   │       ├── img_00001.jpg
    │   │       ├── ....
    │   │       ├── img_nnnnn.jpg
    │   └── ...
    └──
```

## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 3 : Séparer les données et générer les fichiers d’annotations

Ici, notre intérêt est de séparer la dataset en 3 sous parties :

Les données d’entraînement qui vont nous permettre d’entraîner l’IA

Les données de validation qui vont permettre de vérifier pendant l’entraînement que le modèle ne fait pas du sur-apprentissage. En effet, le modèle peut apprendre si bien les données qu’il ne reconnaîtrait que ces dernières. Cette méthode de validation permet d’ajuster automatiquement les hyperparamètres du modèle afin de préserver la véracité de son entraînement.

Les données de test vont permettre de vérifier la performance du modèle à la fin de l’entraînement.

Il nous suffira ici de rentrer la commande python datasplit_generation.py dataset_name afin de stratifier le jeu de données de la manière suivante : 

* 70% train
* 20% validation 
* 10% test


## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 3 : Séparer les données et générer les fichiers d’annotations

On peut ainsi constater la création de 3 fichiers d’annotations (txt) qui représente pour chaque jeu de données, le dossier du clip, le nombre de frames et la classe respective de la séquence vidéo :

```
class_00/cam_1_clip_2_30f 290 0
class_09/cam_2_clip_3_f30 321 9
class_05/cam_1_clip_2_f30 475 5
class_00/cam_2_clip_0_30f 400 0
class_10/cam_2_clip_2_f30 680 10
```

On peut également créer dans data/dataset_name le fichier labels.txt qui liste les classes d’actions que l’on utilisera :

```
0_zen_0_nothing
0_zen_1_presence
1_rescue_2_attirer
1_rescue_3_couper
2_time_to_eat_4_banc_poissons
3_face_to_face_5_attirer
3_face_to_face_6_position
```

## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 4 : Modifier les fichiers de configuration du jeu de données

Le fichier de configuration est celui qui décrit notre réseau de neurones path = configs/recognition/i3d/your_config.py. 

Il va préciser le nombre de couches, les paramètres d’apprentissage, les métriques de performance, le nombre d’époques d'entraînement (nombre de fois où l’IA parcourt l’ensemble de la base de données d'entraînement), …

Afin de lui indiquer notre base de données il faut lui préciser le chemin de celle-ci :

```
dataset_type = 'RawframeDataset'
data_root = 'data/dataset_name/rawframes/'
data_root_val = 'data/dataset_name/rawframes/'
ann_file_train = 'data/dataset_name/annotation_train.txt'
ann_file_val = 'data/dataset_name/annotation_valid.txt'
ann_file_test = 'data/dataset_name/annotation_test.txt'
```

## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 4 : Modifier les fichiers de configuration de la dataset

Afin d’accélérer l’entraînement, nous utilisons une technique de fine-tuning qui consiste à démarrer à partir des poids d’un modèle pré-entraîné sur un grand jeu de données. On peut alors adapter notre modèle avec cet entraiîement pour nos données spécifiques. Nous avons choisi de démarrer avec un pré-entraînement sur la base de données Kinetics-400 car elle est très populaire en HAR. On peut modifier ce raffinement dans la configuration et définir le dossier de sortie d’entraînement :
```
work_dir = 'work_dirs/2022-2023-CREO-Megaceta_i3d'
load_from = './checkpoints/i3d_r50_32x2x1_100e_kinetics400_rgb/i3d_r50_32x2x1_100e_kinetics400_rgb_20200614-c25ef9a4.pth'
```

## TUTORIEL MMACTION2: Étapes d'entraînement

### Étape 5 : Entraînement et test du modèle

Dans ~/livepose/mmaction2-creo:
```
source ~/livepose/mmaction2_venv/bin/activate
export PATH=/usr/local/cuda-11.7/bin:$PATH
Nous pouvons enfin lancer l’entraînement du modèle à l’aide de la commande : 
python tools/train.py path_to_config_file –validate –deterministic --test-best --work-dir output_path
```

Ceci nous donnera le résultat d’entraînement dans le fichier work_dirs/output_path/best.pth

Enfin, afin de vérifier la performance, on peut tester avec :
```
python tools/test.py path_to_config_file_under_workdirectory path_to_saved_checkpoints_under_work_directory –eval mean_class_accuracy top_k_accuracy –out path_to_json
```

Enfin, nous pouvons générer la matrice de confusion du modèle, celle-ci permet l’analyse des erreurs et du pourcentage de performance. 

On lance la commande : 
```
python tools/analysis/confusion_matrix.py path_to_output.json path_to_annotation_test.txt path_to_save_matrix_image title_for_confusion_matrix
```

## TUTORIEL MMACTION2: Run in LivePose

Dans ~/livepose, on prépare le fichier de configuration livepose/livepose/configs/frame_liveaction.json en modifiant :

```
"filters": {
    	"skeletons": {},
    	"frame_action_detection": {
        	"action_detection_config_path": "path_to_your_config.py",
        	"label_map_path": "path_to_your_labels.txt",
        	"camera_resolution": [640, 480],
        	"device": "cuda:0",
        	"average_size": 3,
        	"threshold": 0.7,
        	"inference_fps": 0,
        	"checkpoint_url": "path_to_your_checkpoint_output.pth"
    	}
	},
```

## TUTORIEL MMACTION2: Run in LivePose

On peut ajouter une détection de pose en modifiant le pose_backends (ici mediapipe) et ainsi récupérer la position de la personne et de ses mains en temps réel :

```
	"pose_backends": [
    	{
        	"name": "mediapipe",
        	"params": {
            	"detect_faces": false,
            	"detect_face_meshs": false,
            	"detect_hands": true,
            	"detect_poses": true,
            	"detect_holistic": false,
            	"use_gpu": true,
            	"min_detection_confidence": 0.5
        	}
    	}
	],
```

Enfin, on peut lancer cette configuration avec notre modèle en entrant la commande : 

```
./livepose.sh -c livepose/configs/frame_liveaction.json
```

On voit alors sur l’écran la détection de pose et sur le terminal la détection d’action.

## TUTORIEL MMACTION2: Output

L’envoi de ces données peut être fait en OSC ou en websocket sur LivePose.

## Références supplémentaires

exemple avec JavaScript et messages OSC 

* https://sat-mtl.gitlab.io/metalab/blog/fr/posts/livepose-ballant.html
* https://gitlab.com/sat-mtl/tools/livepose/-/tree/main/examples/ballant

